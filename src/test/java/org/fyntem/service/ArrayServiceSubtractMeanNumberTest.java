package org.fyntem.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class ArrayServiceSubtractMeanNumberTest {
    @Test
    public void testSubtractMeanNumberFromArrayElements1() {
        int[] incomeArray = new int[] {4,8,-5,24,-14,10,33,9,1,0,5,5,15,19,3,20};
        int[] resultArray = ArrayService.subtractMeanNumberFromArrayElements(incomeArray);

        assertArrayEquals(new int[] {-5,-1,-14,15,-23,1,24,0,-8,-9,-4,-4,6,10,-6,11}, resultArray);
    }

    @Test
    public void testSubtractMeanNumberFromArrayElements2() {
        int[] incomeArray = new int[] {-5,-5,-5,-5,-5,-5,-5};
        int[] resultArray = ArrayService.subtractMeanNumberFromArrayElements(incomeArray);

        assertArrayEquals(new int[] {0, 0, 0, 0, 0, 0, 0}, resultArray);
    }

    @Test
    public void testSubtractMeanNumberFromArrayElements3() {
        int[] incomeArray = new int[] {0};
        int[] resultArray = ArrayService.subtractMeanNumberFromArrayElements(incomeArray);

        assertArrayEquals(new int[] {0}, resultArray);
    }
}
