package org.fyntem.service;

import org.fyntem.service.ArrayService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class ArrayServiceExcludeThirdElementTest {
    @Test
    public void testExcludeEveryThirdElementOfAnArray1() {
        int[] incomeArray = new int[] {9, 4, -3, -2, 13, 27, 1, 9, 100, 10};
        int[] resultArray = ArrayService.excludeEveryThirdElementOfAnArray(incomeArray);

        assertArrayEquals(new int[] {9, 4, -2, 13, 1, 9, 10}, resultArray);
    }

    @Test
    public void testExcludeEveryThirdElementOfAnArray2() {
        int[] incomeArray = new int[] {1, 1, 1, 1, 1};
        int[] resultArray = ArrayService.excludeEveryThirdElementOfAnArray(incomeArray);

        assertArrayEquals(new int[] {1, 1, 1, 1}, resultArray);
    }

    @Test
    public void testExcludeEveryThirdElementOfAnArray3() {
        int[] incomeArray = new int[] {0};
        int[] resultArray = ArrayService.excludeEveryThirdElementOfAnArray(incomeArray);

        assertArrayEquals(new int[] {0}, resultArray);
    }
}
