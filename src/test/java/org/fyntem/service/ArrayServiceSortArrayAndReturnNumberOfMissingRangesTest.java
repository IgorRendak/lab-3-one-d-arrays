package org.fyntem.service;

import org.fyntem.service.ArrayService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

public class ArrayServiceSortArrayAndReturnNumberOfMissingRangesTest {
    @Test
    public void testSortArrayAndReturnNumberOfMissingRangesOfElements1() {
        int[] incomeArray = new int[] {2,1,10,4,5,7};
        int[] resultArray = ArrayService.sortArrayAndReturnNumberOfMissingRangesOfElements(incomeArray);

        assertArrayEquals(new int[] {1,1,2}, resultArray);
    }

    @Test
    public void testSortArrayAndReturnNumberOfMissingRangesOfElements2() {
        int[] incomeArray = new int[] {-3,-6,0,4};
        int[] resultArray = ArrayService.sortArrayAndReturnNumberOfMissingRangesOfElements(incomeArray);

        assertArrayEquals(new int[] {2,2,3}, resultArray);
    }

    @Test
    public void testSortArrayAndReturnNumberOfMissingRangesOfElements3() {
        int[] incomeArray = new int[] {5,5,5,5,5};
        int[] resultArray = ArrayService.sortArrayAndReturnNumberOfMissingRangesOfElements(incomeArray);

        assertArrayEquals(new int[] {0}, resultArray);
    }

    @Test
    public void testSortArrayAndReturnNumberOfMissingRangesOfElements4() {
        int[] incomeArray = new int[] {-2,-1,-3,3,1,2,0};
        int[] resultArray = ArrayService.sortArrayAndReturnNumberOfMissingRangesOfElements(incomeArray);

        assertArrayEquals(new int[] {0}, resultArray);
    }

    @Test
    public void testSortArrayAndReturnNumberOfMissingRangesOfElements5() {
        int[] incomeArray = new int[] {0};
        int[] resultArray = ArrayService.sortArrayAndReturnNumberOfMissingRangesOfElements(incomeArray);

        assertArrayEquals(new int[] {0}, resultArray);
    }
}
