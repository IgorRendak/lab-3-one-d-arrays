package org.fyntem.service;

import java.util.Arrays;

public class ArrayService {

    /**
     * Given array of elements A = {a0, a1, a2, ... , ak}, k ∈ N, ai ∈ Z
     * @return new array of 2 elements B = {b0, b1} where b0 = min(a0, a2, a4, ... , a2l)
     * and b1 = max(a1, a3, a5, ... , a(2m-1));
     * l,m < k
     *
     * Requirements:
     * - If there are less than 2 numbers in income array - throw IllegalArgumentException.
     */
    public static int[] calculateMinAndMaxElementsOfSubArrays(int[] incomeArray) {
        if (incomeArray.length < 2){
            throw new IllegalArgumentException("Array must have at least 2 numbers");
        }
        int min = incomeArray[0];
        int max = incomeArray[1];

        for (int i = 2; i < incomeArray.length; i += 2)

        {
            if (incomeArray[i] < min) {
                min = incomeArray[i];
            }
        }
        for (int i = 3; i < incomeArray.length; i += 2){
            if (incomeArray[i] > max){
                max = incomeArray[i];
            }
        }
        return new int[] {min, max};
    }

    /**
     * Given array of elements A = {a0, a1, a2, ... , ak}, k ∈ N, ai ∈ Z
     * @return new array where every third element is excluded starting with the beginning
     * B = {a0, a1, a3, a4, a6, a7, ... a(k-2), a(k-1)}
     */
    public static int[] excludeEveryThirdElementOfAnArray(int[] incomeArray) {
        int newArray = incomeArray.length - (incomeArray.length / 3);
        int[] sortedArray = new int[newArray];
        int j = 0;
        for (int i = 0; i < incomeArray.length; i++) {
            if ((i + 1) % 3 != 0) {
                sortedArray[j++] = incomeArray[i];
            }
        }
        return sortedArray;
    }

    /**
     * Given array of elements A = {a0, a1, a2, ... , ak}, k ∈ N, ai ∈ Z
     * @return new array in which average number of this array is subtracted from every element.
     * M - mean (average) number = [(∑ai)/k], k - number of elements in array, ∑ai - sum of all elements
     * [] - whole number of result
     * B = {a0 - M, a1 -M, a2 -M , ... ak - M}
     *
     * Example:
     * A = [1,5,3,23,6,3,3,7,9,15,-4,-6]
     * Sum of elements = 65
     * Number of elements k = 12
     * Mean number = 65/12 = 5.41666666667
     * Whole part of mean number = 5
     * result B = [-4,0,-2,18,1,-2,-2,2,4,10,-9,-11]
     *
     * Requirements:
     * - If you round numbers, use round up (e.g. 4.56 = 5, 2.1 = 2, 9.5 = 10)
     */
    public static int[] subtractMeanNumberFromArrayElements(int[] incomeArray) {
        int sum = 0;
        double mean;
        for (int j : incomeArray) {
            sum += j;
        }
        mean = (double) sum / incomeArray.length;
        int intMean = (int) Math.round(mean);
        int[] resultArray = new int[incomeArray.length];
        for (int i = 0; i < incomeArray.length; i++) {
            resultArray[i] = incomeArray[i] - intMean;
        }
        return resultArray;
    }

    /**
     * Given array of elements A = {a0, a1, a2, ... , ak}, k ∈ N, ai ∈ Z
     * @return new array B which has number of missing whole numbers in a range.
     * Example:
     * Income array = [2,1,10,4,5,7]
     * Sorted array = [1,2,4,5,7,10]
     * Missing ranges = [3], [6], [8,9]
     * result B = [1,1,2] as number of missing ranges in sorted array.
     *
     * Requirements:
     * - If there are no any ranges between sorted numbers - return array with single 0 in it.
     */
    public static int[] sortArrayAndReturnNumberOfMissingRangesOfElements(int[] incomeArray) {
        int[] sortedArray = new int[incomeArray.length];
        System.arraycopy(incomeArray, 0, sortedArray, 0, incomeArray.length);

        for (int i = 0; i < sortedArray.length; i++) {
            for (int j = i + 1; j < sortedArray.length; j++) {
                if (sortedArray[i] > sortedArray[j]) {
                    int temp = sortedArray[i];
                    sortedArray[i] = sortedArray[j];
                    sortedArray[j] = temp;
                }
            }
        }
        int[] missingRanges = new int[incomeArray.length];
        int index = 0;

        int prev = sortedArray[0] - 1;
        for (int curr : sortedArray) {
            if (curr - prev > 1) {
                missingRanges[index++] = curr - prev - 1;
            }
            prev = curr;
        }

        if (index == 0) {
            return new int[] {0};
        } else {
            int[] resultArray = new int[index];
            System.arraycopy(missingRanges, 0, resultArray, 0, index);
            return resultArray;
        }
    }

    /**
     * Given array of elements A = {a0, a1, a2, ... , ak}, k ∈ N, ai ∈ Z
     * and real number r ∈ R.
     * @return new array B = {b0,b1} which has the closest numbers to a given real number
     * (b0 - closest before real number, b1 - closest after the given real number)
     *
     * Example:
     * Income array = [2,1,10,4,5,7]
     * Real number = 8.17
     * result B = [7,10]
     *
     * Requirements:
     * - If you round numbers, use round up (e.g. 4.56 = 5, 2.1 = 2, 9.5 = 10)
     * - If there are several identical closest numbers - use only 1 of each of them
     * - Lower number should go first in a result array.
     * - If there are no lower or higher number for the real one in income array -
     * print instead of that number -256 (for lower) or 256 (for higher).
     */

    public static int[] findElementsInAnArrayThatAreClosestToTheRealNumber(int[] incomeArray, double realNumber) {
        int[] resultArray = new int[2];
        Arrays.sort(incomeArray);

        int lowerIndex = -1;
        int higherIndex = incomeArray.length;

        for (int i = 0; i < incomeArray.length; i++) {
            if (incomeArray[i] <= realNumber) {
                lowerIndex = i;
            }
            if (incomeArray[i] >= realNumber) {
                higherIndex = i;
                break;
            }
        }

        if (lowerIndex == -1) {
            resultArray[0] = -256;
        } else {
            resultArray[0] = incomeArray[lowerIndex];
        }

        if (higherIndex == incomeArray.length) {
            resultArray[1] = 256;
        } else {
            resultArray[1] = incomeArray[higherIndex];
        }

        if (lowerIndex == higherIndex) {
            resultArray[0] = incomeArray[lowerIndex];
        }

        return resultArray;
    }

}
